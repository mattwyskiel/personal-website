const child = require('child_process');
const browserSync = require('browser-sync').create();

const gulp = require('gulp');
const concat = require('gulp-concat');
const gutil = require('gulp-util');
const sass = require('gulp-sass');

const autoprefixer = require('gulp-autoprefixer');

const siteRoot = '_site';
const cssFiles = '_sass/**';
const mainCSS = '_sass/main.sass';

gulp.task('css', (cb) => {
  gulp.src(mainCSS)
    .pipe(sass())
    .pipe(concat('main.css'))
    .pipe(autoprefixer())
    .pipe(gulp.dest('assets'));
  cb();
});

gulp.task('jekyll', () => {
  const jekyll = child.spawn('jekyll', ['build',
    '--watch',
    '--incremental',
    '--drafts'
  ]);

  const jekyllLogger = (buffer) => {
    buffer.toString()
      .split(/\n/)
      .forEach((message) => gutil.log('Jekyll: ' + message));
  };

  jekyll.stdout.on('data', jekyllLogger);
  jekyll.stderr.on('data', jekyllLogger);

  return jekyll;
});

gulp.task('serve', (cb) => {
  browserSync.init({
    files: [siteRoot + '/**'],
    port: 4000,
    server: {
      baseDir: siteRoot
    }
  });

  gulp.watch(cssFiles, gulp.series(['css']));
  cb();
});

exports.default = gulp.parallel(['css', 'jekyll', 'serve'])